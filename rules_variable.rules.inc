<?php

/**
 * Rules actions for variables/strongarm.
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_variable_rules_action_info() {
  // Add actions that depend on required modules only.
  $actions = array(
    'variable_get' => array(
      'label' => t('Get variable'),
      'parameter' => array(
        'name' => array(
          'type' => 'text',
          'label' => t('Name'),
          'description' => t('Name of the variable to get.'),
          'restriction' => 'input',
        ),
        'precision' => array(
          'type' => 'text',
          'label' => t('Default'),
          'description' => t('Default value for the variable, if no value is stored.'),
        ),
      ),
      'group' => t('Variable'),
      'base' => 'rules_variable_action_get',
      'provides' => array(
        'value' => array(
          'type' => 'text',
          'label' => t('Variable value'),
        ),
      ),
    ),
  );
  return $actions;
}

/**
 * Action callback for getting a variable.
 *
 * @param string $name
 *   Name of the variable.
 * @param string $default
 *   Default value for the variable.
 *
 * @return array
 *   Contains the value of the requested variable.
 */
function rules_variable_action_get($name, $default) {
  $value = (string) variable_get($name, $default);
  return array(
    'value' => $value,
  );
}
